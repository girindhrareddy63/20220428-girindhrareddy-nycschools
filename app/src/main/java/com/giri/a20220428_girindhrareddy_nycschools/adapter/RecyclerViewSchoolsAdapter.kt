package com.giri.a20220426_girindhrareddy_nycschools.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.giri.a20220426_girindhrareddy_nycschools.model.SATScore
import com.giri.a20220426_girindhrareddy_nycschools.model.Schools
import com.giri.a20220428_girindhrareddy_nycschools.MainActivity
import com.giri.a20220428_girindhrareddy_nycschools.R

class RecyclerViewSchoolsAdapter(val schoolsList:List<Schools>,val context:Context): RecyclerView.Adapter<RecyclerViewSchoolsAdapter.MyViewAdapter>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewAdapter {
        val view = LayoutInflater.from(context).inflate(R.layout.school_item,parent,false)
        return MyViewAdapter(view)
    }

    override fun onBindViewHolder(holder: MyViewAdapter, position: Int) {
        holder.bind(schoolsList[position],context as Activity)
    }

    override fun getItemCount(): Int {
          return schoolsList.size
    }
    class MyViewAdapter(view:View):RecyclerView.ViewHolder(view){
        val sName =view.findViewById<AppCompatTextView>(R.id.atv_schoolName)
        val sDesc =view.findViewById<AppCompatTextView>(R.id.atv_overView)
        val sButton =view.findViewById<AppCompatButton>(R.id.act_button)
        fun bind(schools: Schools,activity: Activity){
            sName.text=schools.school_name
            sDesc.text=schools.dbn

                sButton.setOnClickListener {
                    var satScore = MainActivity.satList.find { it.dbn==sDesc.text }

                    if (satScore != null) {
                        showAlertDialog(satScore,activity)
                    }


            }

        }
        fun showAlertDialog(satScore:SATScore,activity: Activity){
            val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
            builder.setTitle("SAT Details")
            // set the custom layout
            val customLayout = activity.layoutInflater.inflate(R.layout.sat_alert_dialog, null)
            customLayout.findViewById<AppCompatTextView>(R.id.atv_schoolName).text=satScore.school_name
            customLayout.findViewById<AppCompatTextView>(R.id.atv_math_avg_score).text=satScore.sat_math_avg_score
            customLayout.findViewById<AppCompatTextView>(R.id.atv_name_sat_test_taker).text=satScore.num_of_sat_test_takers
            customLayout.findViewById<AppCompatTextView>(R.id.atv_reading_score).text=satScore.sat_critical_reading_avg_score
            customLayout.findViewById<AppCompatTextView>(R.id.atv_writing_avg_score).text=satScore.sat_writing_avg_score
            builder.setView(customLayout)
            builder.setPositiveButton("Ok",object :DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                     p0!!.cancel()
                }

            })
            val dialog = builder.create()
            dialog.show()

        }
    }

}