package com.giri.a20220426_girindhrareddy_nycschools.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitObj {
    fun getInstance():Retrofit
    {
        return Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    }


}