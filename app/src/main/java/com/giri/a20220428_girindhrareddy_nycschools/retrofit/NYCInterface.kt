package com.giri.a20220426_girindhrareddy_nycschools.retrofit

import com.giri.a20220426_girindhrareddy_nycschools.model.SATScore
import com.giri.a20220426_girindhrareddy_nycschools.model.Schools
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface NYCInterface {
        @GET("/resource/s3k6-pzi2.json")
        fun getNycSchools():Call<List<Schools>>

        @GET("/resource/f9bf-2cp4.json")
        fun getSATScores():Call<List<SATScore>>

}