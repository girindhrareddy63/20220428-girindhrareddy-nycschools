package com.giri.a20220426_girindhrareddy_nycschools.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.giri.a20220426_girindhrareddy_nycschools.model.SATScore
import com.giri.a20220426_girindhrareddy_nycschools.model.Schools
import com.giri.a20220426_girindhrareddy_nycschools.retrofit.NYCInterface
import com.giri.a20220426_girindhrareddy_nycschools.retrofit.RetrofitObj
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class MainViewModel: ViewModel() {
   lateinit var schoolsLiveData:MutableLiveData<List<Schools>>
   lateinit var SATScoreLiveData:MutableLiveData<List<SATScore>>
    init {
        schoolsLiveData= MutableLiveData()
        SATScoreLiveData = MutableLiveData()
    }
    val retroInstance =  RetrofitObj.getInstance().create(NYCInterface::class.java)
    fun callSchools(){
        retroInstance.getNycSchools().enqueue(object :retrofit2.Callback<List<Schools>>{
            override fun onResponse(call: Call<List<Schools>>, response: Response<List<Schools>>) {
                schoolsLiveData.value = response.body()
            }

            override fun onFailure(call: Call<List<Schools>>, t: Throwable) {
                Log.d("Error:",t.message!!)
            }

        })
    }
    fun callSATScore(){
        retroInstance.getSATScores().enqueue(object :retrofit2.Callback<List<SATScore>>{
            override fun onResponse(
                call: Call<List<SATScore>>,
                response: Response<List<SATScore>>
            ) {
                SATScoreLiveData.value = response.body()
            }
            override fun onFailure(call: Call<List<SATScore>>, t: Throwable) {
                Log.d("Error:",t.message!!)
            }
        })
    }
}