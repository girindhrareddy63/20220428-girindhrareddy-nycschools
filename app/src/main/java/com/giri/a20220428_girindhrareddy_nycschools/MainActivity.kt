package com.giri.a20220428_girindhrareddy_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.giri.a20220426_girindhrareddy_nycschools.adapter.RecyclerViewSchoolsAdapter
import com.giri.a20220426_girindhrareddy_nycschools.model.SATScore
import com.giri.a20220426_girindhrareddy_nycschools.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {
    lateinit var mainViewModel: MainViewModel
    lateinit var recyclerView: RecyclerView
    lateinit var recyclerViewAdapter: RecyclerViewSchoolsAdapter
    companion object{
        lateinit var satList:List<SATScore>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        mainViewModel.callSchools()
        mainViewModel.callSATScore()
        mainViewModel.schoolsLiveData.observe(this, Observer {
            recyclerViewAdapter=RecyclerViewSchoolsAdapter(it,this)
            recyclerView.adapter=recyclerViewAdapter
        })
        mainViewModel.SATScoreLiveData.observe(this, Observer {
            satList=it
        })
    }

    private fun init(){
        recyclerView = findViewById(R.id.rv_schools)
        recyclerView.layoutManager= LinearLayoutManager(this)
    }
}